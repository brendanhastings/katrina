
$(document).ready(function() {
    
    // ----
    // Toggle Menu
    // ----

    // TODO: better fallback for non-JS - adding a .js class but it causes the nav to blink
    // Look into Modernizr for that

    var $menu = $('#menu'),
        $menulink = $('.menu-link');

    $menulink.on('click', function(e) {
        e.preventDefault();
        $menulink.toggleClass('active');
        $menu.toggleClass('active');
        return false;
    });


    // Close menu on body click and escape - this is spaghetti as well.
    $('body').on( 'click', function(){
        if ($menu.hasClass('active')) {
            $menu.removeClass('active');
        }
    });
    
    $(document).on( 'keyup', function(e) {
        if (e.keyCode == 27) {
            if ($menu.hasClass('active')) {
                $menu.removeClass('active');
            }
        }
    });



    // ----
    // Plugins
    // ----

    $('.site__main').fitVids({ customSelector: "iframe" });
    
    $('.header__hero--slider').flickity({
        autoPlay: 9000
    });

    $('.loop__tease--thumb').flickity({
        initialIndex: 1,
        wrapAround: true,
        imagesLoaded: true,
        autoPlay: 1500
    });

    $('.block_gallery__gallery').flickity({
        initialIndex: 1,
        wrapAround: true,
        imagesLoaded: true
    });

    window.lazySizesConfig = {
        addClasses: true
    };




    // ----
    // Smoothly Background Videos
    // ---

    // If there are videos, load them nicely
    // Thx: http://atomicrobotdesign.com/blog/web-development/check-when-an-html5-video-has-loaded/

    if( $('video').length ) {
        window.addEventListener('load', function() {
            var video = document.querySelector('.ambient__video');
            var preloader = document.querySelector('.spinner');

            function checkLoad() {
                if (video.readyState === 4) {
                    // preloader.fadeOut(300);
                    video.play();
                } else {
                    setTimeout(checkLoad, 100);
                }
            }
            checkLoad();
        }, false);
    }





    // ----
    // Scroll to top link
    // ----

    $('.footer--site__nav--backtop a').on( 'click', function() {
        var hash = $('#pageTop');
        var $target = $(hash);

        // Slide to section corresponding to clicked hash
        $('html,body').animate({
            scrollTop: $target.offset().top
        }, 500);

        return false;

    });

    // Fade in backtop link when the header is no longer visible
    $(window).scroll( function() {
        var headerHeight = $('.header').outerHeight();

        if ( $(this).scrollTop() > headerHeight ) {
            $('.footer--site__nav--backtop a').fadeIn(400);
        } else if ($(this).scrollTop() < headerHeight) {
            $('.footer--site__nav--backtop a').fadeOut(400);
        }
    });




    // ----
    // Before and After slider
    // ----

    // Without the animations
    // codyhouse.co/gem/css-jquery-image-comparison-slider/

    // make the .cd-handle element draggable and modify .cd-resize-img width according to its position
    $('.cd-image-container').each(function(){
        var actual = $(this);
        drags(actual.find('.cd-handle'), actual.find('.cd-resize-img'), actual, actual.find('.cd-image-label[data-type="original"]'), actual.find('.cd-image-label[data-type="modified"]'));
    });


    // ----
    // Ajax Load More Posts
    // ----

    // http://premium.wpmudev.org/blog/load-posts-ajax/
    // http://www.problogdesign.com/wordpress/load-next-wordpress-posts-with-ajax/

    // The number of the next page to load (/page/x/).
    // var pageNum = parseInt(ajax_data.startPage) + 1;
 
    // // The maximum number of pages the current query can return.
    // var max = parseInt(ajax_data.maxPages);
 
    // // The link of the next page of posts.
    // var nextLink = ajax_data.nextLink;

    // if(pageNum <= max) {
    
    // // Insert the "More Posts" link.
    // $('.ajaxTesting')
    //     .append('<div class="pbd-alp-placeholder-'+ pageNum +'"></div>')
    //     .append('<div class="btn__wrap"><a href="#" class="btn--more" id="loadMore">Load More</a></div>');
    // }


    // $('#loadMore').on('click', function() {
    //     if(pageNum <= max) {
 
    //         // Show that we're working.
    //         $(this).text('Loading...');

    //         // $.ajax({
    //         //     url: ajax_data.url,
    //         //     type: 'post',
    //         //     data: {
    //         //         query_vars: ajax_data.query_vars,
    //         //         action: 'ajax_pagination',
    //         //         nextLink: ajax_data.nextLink
    //         //     },
    //         //     success: function(html) {
    //         //         $('.ajaxTesting').html(html);
    //         //     }
    //         // });

    //         $('.pbd-alp-placeholder-'+pageNum).load(nextLink + ' .post',
    //             function() {
    //                 pageNum++;
    //                 nextLink = nextLink.replace(/\/page\/[0-9]?/, 'page' + pageNum);

    //                 // Add new placeholder for upcoming posts
    //                 $('#pbd-alp-load-posts')
    //                     .before('<div class="pbd-alp-placeholder-'+ pageNum +'"></div>');
                    
    //                 if(pageNum <= max) {
    //                     $('#pbd-alp-load-posts a').text('Load More Posts');
    //                 } else {
    //                     $('#pbd-alp-load-posts a').text('No more posts to load.');
    //                 }
    //             }
    //         );
    //     } else {
    //         $('#pbd-alp-load-posts a').append('.');
    //     }

    //     return false;
    // });

});

// draggable funtionality - credits to http://css-tricks.com/snippets/jquery/draggable-without-jquery-ui/
function drags(dragElement, resizeElement, container, labelContainer, labelResizeElement) {
    dragElement.on("mousedown vmousedown", function(e) {
        dragElement.addClass('draggable');
        resizeElement.addClass('resizable');

        var dragWidth = dragElement.outerWidth(),
            xPosition = dragElement.offset().left + dragWidth - e.pageX,
            containerOffset = container.offset().left,
            containerWidth = container.outerWidth(),
            minLeft = containerOffset - 12,
            maxLeft = containerOffset + containerWidth - dragWidth + 22;
        
        dragElement.parents().on("mousemove vmousemove", function(e) {
            leftValue = e.pageX + xPosition - dragWidth;
            
            //constrain the draggable element to move inside his container
            if(leftValue < minLeft ) {
                leftValue = minLeft;
            } else if ( leftValue > maxLeft) {
                leftValue = maxLeft;
            }

            widthValue = (leftValue + dragWidth/2 - containerOffset)*100/containerWidth+'%';
            
            $('.draggable').css('left', widthValue).on("mouseup vmouseup", function() {
                $(this).removeClass('draggable');
                resizeElement.removeClass('resizable');
            });

            $('.resizable').css('width', widthValue); 
             
        }).on("mouseup vmouseup", function(e){
            dragElement.removeClass('draggable');
            resizeElement.removeClass('resizable');
        });
        e.preventDefault();
    }).on("mouseup vmouseup", function(e) {
        dragElement.removeClass('draggable');
        resizeElement.removeClass('resizable');
    });
}
