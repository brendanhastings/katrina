module.exports = function(grunt, config) {
    
    var jsFileList = [
    	config.jsConcatDir + 'modernizr-custom.js',
		config.jsSrcDir + 'bower.js',
		config.jsSrcDir + '_main.js'
    ];  

    console.log(jsFileList);

	grunt.config.merge({
		
		jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
        
            all: [
                'Gruntfile.js',
                config.jsSrcDir + '*.js'
            ],
        },
        
		// bower_concat: {
		// 	all: {
		// 	    dest: config.jsSrcDir + 'bower.js',
		// 	    cssDest: config.cssDir + 'bower.css',
		// 	    exclude: [
		// 			'jquery',
		// 	      	'modernizr',
		// 	      	'flickity'
		// 	    ]
		// 	}
		// },

		concat: {
			js: {
				src: [
		            	// config.jsConcatDir + 'modernizr-custom.js',
						config.jsSrcDir + 'fitvids.min.js',
						config.jsSrcDir + 'jquery-mobile.custom.js',
						config.jsSrcDir + 'flickity.pkgd.min.js',
						config.jsSrcDir + '_main.js'
	            	],
	            	dest: config.jsConcatDir + 'scripts.js'
	            }
		},
		
	    uglify: {
	    	options: {
	    	},
			dist: {
	            files: {
					// For some reason this doesn't accept the config variable for the key. Bleh.
					'js/build/scripts.min.js': ['<%= concat.js.dest %>']
	            }
	        }
	    },

	    watch: {
	    	js: {
				files: jsFileList,
				tasks: [
					'concat'
				]
	    	}
	    }

	});

}

