<?php


	if (!class_exists('Timber')){
		add_action( 'admin_notices', function(){
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . admin_url('plugins.php#timber') . '">' . admin_url('plugins.php') . '</a></p></div>';
		});
		return;
	}

	class StarterSite extends TimberSite {

		function __construct(){
			add_theme_support('post-formats');
			add_theme_support('post-thumbnails');
			add_theme_support('menus');
			add_filter('timber_context', array($this, 'add_to_context'));
			add_filter('get_twig', array($this, 'add_to_twig'));
			add_action('init', array($this, 'register_post_types'));
			add_action('init', array($this, 'register_taxonomies'));
			add_action('init', array($this, 'twc_acf_utils'));
			parent::__construct();
		}


		// Note that the following included files only need to contain the taxonomy/CPT/Menu arguments and register_whatever function. They are initialized here.
		// http://generatewp.com is nice
		
		function register_post_types(){
			require('lib/custom-types.php');
		}

		function register_taxonomies(){
			require('lib/taxonomies.php');
		}

		function register_menus() {
			require('lib/menus.php');
		}

		function twc_acf_utils() {
			require('lib/acf-utils.php');
		}

		function add_to_context($context){

			$context['menu'] = new TimberMenu();
			$context['site'] = $this;

			global $post;

			if (!is_404()) {
				$context['current_id'] = $post->ID;
			}


			/*
			 *
			 * Site-wide Settings Context
			 *
			 */

			// Footer colophon - not really using
			
			$context['site_footer_copyright'] = get_field('site_footer_copyright', 'options');
			$context['site_footer_credits'] = get_field('site_footer_credits', 'options');

			// Header
			$context['logo_left'] = get_field('site_left_logo', 'options');
			$context['logo_right'] = get_field('site_right_logo', 'options');
			$context['logo_right_link'] = get_field('site_right_logo_link', 'options');
			$context['twitter_link'] = get_field('twitter_link', 'options');
			$context['facebook_link'] = get_field('facebook_link', 'options');

			// Homepage
			$context['thumb_cols'] = get_field('homepage_columns', 'options');

			// Favicon
			$context['favicon_16'] = get_field('favicon_16', 'options');
			$context['favicon_32'] = get_field('favicon_32', 'options');

			// Chapters loop in footer
			$thumbs_num = get_field('number_of_thumbs', 'options');

			$exclude_args = array(
				'post_type' => 'chapter',
				'fields' => 'ids',
				'meta_query' => array(
					'relation' => 'AND',
					array (
						'key' => 'show_in_chapter_feed',
					    'value' => '0',
					    'compare' => '='
					)
			    )
			);

			$exclude_chapters = get_posts($exclude_args);
			
			$chapter_args = array(
				'post_type' => 'chapter',
				'post__not_in' => $exclude_chapters
				// 'posts_per_page' => $thumbs_num
			);

			$context['chapters'] = Timber::get_posts($chapter_args);

			return $context;
		}

		function add_to_twig($twig){
			/* this is where you can add your own fuctions to twig */
			$twig->addExtension(new Twig_Extension_StringLoader());
			return $twig;
		}

	}

	new StarterSite();


	
	/*
	 **************************
	 * Custom Theme Functions *
	 **************************
	 *
	 * Namespaced "twc" - find and replace with your own three-letter-thing.
	 * 
	 */ 

	// Enqueue scripts
	function twc_scripts() {

		// Use jQuery from CDN, enqueue in footer
		// Add Picturefill
		if (!is_admin()) {
			wp_deregister_script('jquery');
			
			wp_register_script('jquery', '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js', array(), null, true);
			wp_register_script('picturefill', get_template_directory_uri() . '/assets/js/build/picturefill.min.js', array(), null);
			wp_register_script('lazysizes', get_template_directory_uri() . '/assets/js/src/lazysizes.min.js', array(), null);
			
			wp_enqueue_script('picturefill');
			wp_enqueue_script('lazysizes');
		}

		// Enqueue stylesheet and scripts. Use minified for production.
		if( WP_ENV == 'production' ) {
			wp_enqueue_style( 'twc-styles', get_template_directory_uri() . '/assets/css/build/main.min.css', 1.0);
			wp_enqueue_script( 'twc-js', get_template_directory_uri() . '/assets/js/build/scripts.min.js', array('jquery'), '1.0.0', true );
		} else {
			wp_enqueue_style( 'twc-styles', get_stylesheet_directory_uri() . '/assets/css/build/main.css', 1.0);
			wp_enqueue_script( 'twc-js', get_template_directory_uri() . '/assets/js/build/scripts.js', array('jquery'), '1.0.0', true );
		}
		
		// Localize some data for use in JS
		$thumbs_num = get_field('number_of_thumbs', 'options');
		$chapter_args = array(
			'post_type' => 'chapter',
			'posts_per_page' => $thumbs_num
		);

		$chapters = new WP_Query( $chapter_args );

		$max = $chapters->max_num_pages;
		$paged = ( get_query_var('paged') > 1 ) ? get_query_var('paged') : 1;
 		

		wp_localize_script( 'twc-js', 'ajax_data', array(
			'url' => admin_url( 'admin-ajax.php' ),
			'query_vars' => json_encode( $chapters ),
			'startPage' => $paged,
			'maxPages' => $max,
			'nextLink' => next_posts($max, false)
		));
	}
	add_action( 'wp_enqueue_scripts', 'twc_scripts' );

	
	// Remove slug from Chapter custom type. This seems too hacky, but okay for now?
	// http://www.markwarddesign.com/2014/02/remove-custom-post-type-slug-permalink/

	function vipx_remove_cpt_slug( $post_link, $post, $leavename ) {
	 
	    if ( ! in_array( $post->post_type, array( 'chapter' ) ) || 'publish' != $post->post_status )
	        return $post_link;
	 
	    $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
	 
	    return $post_link;
	}
	add_filter( 'post_type_link', 'vipx_remove_cpt_slug', 10, 3 );
	 
	function vipx_parse_request_tricksy( $query ) {
	 
	    // Only noop the main query
	    if ( ! $query->is_main_query() )
	        return;
	 
	    // Only noop our very specific rewrite rule match
	    if ( 2 != count( $query->query )
	        || ! isset( $query->query['page'] ) )
	        return;
	 
	    // 'name' will be set if post permalinks are just post_name, otherwise the page rule will match
	    if ( ! empty( $query->query['name'] ) )
	        $query->set( 'post_type', array( 'post', 'chapter', 'page' ) );
	}
	add_action( 'pre_get_posts', 'vipx_parse_request_tricksy' );


	 
	// Ajax Load More
    
    // References:
    // http://premium.wpmudev.org/blog/load-posts-ajax/
    // http://www.problogdesign.com/wordpress/load-next-wordpress-posts-with-ajax/


	add_action( 'wp_ajax_nopriv_ajax_pagination', 'my_ajax_pagination' );
	add_action( 'wp_ajax_ajax_pagination', 'my_ajax_pagination' );

	function my_ajax_pagination() {

	    $query_vars = json_decode( stripslashes( $_POST['query_vars'] ), true );
	    $query = new WP_Query( $query_vars );

	    // $GLOBALS['wp_query'] = $query;
	 //    echo '<h1>$query</h1>';
	 //    echo '<pre>';
	 //    print_r($query);
		// echo '</pre>';

	 	if( ! $query->have_posts() ) { 
	    }
	    else {
	        while ( $posts->have_posts() ) { 
	            $posts->the_post();
	            echo 'hi';
		        echo '<h5>' . the_title() . '</h5>';
	            // get_template_part( 'content', get_post_format() );
	        }
	    }

	    echo $_POST['nextLink'];

		// if ( $query->have_posts() ) {
		// 	while ( $query->have_posts() ) {
		// 		$query->the_post(); 
		// 		echo '<h5>';
		// 		echo $_POST['title'];
		// 		echo '</h5>';
		// 	} // end while
		// } // end if

	    die();
	}



	/* 
	 * 
	 * Nice to Haves
	 *
	 */


	// Change Title field placeholders for Custom Post Types
	// (You'll need to register the types, of course)

	function twc_title_placeholder_text ( $title ) {
		if ( get_post_type() == 'chapter' ) {
			$title = __( 'Chapter Title' );
		}
		return $title;
	} 
	add_filter( 'enter_title_here', 'twc_title_placeholder_text' );



	// Customize the editor style
	// It's just the Bootstrap typography, but I like it. Got the idea from Roots.io.
	
	function twc_editor_styles() {
		add_editor_style( 'assets/css/editor-style.css' );
	}
	add_action( 'after_setup_theme', 'twc_editor_styles' );



	// Add excerpts to pages
	function twc_add_excerpts_to_pages() {
		add_post_type_support( 'page', 'excerpt' );
	}
	add_action( 'init', 'twc_add_excerpts_to_pages' );
	


	// Remove inline gallery styles
	add_filter( 'use_default_gallery_style', '__return_false' );





	/*
	 * 
	 * Plugin Helpers
	 *
	 */



	// Make custom fields work with Yoast SEO (only impacts the light, but helpful!)
	// https://imperativeideas.com/making-custom-fields-work-yoast-wordpress-seo/

	if ( is_admin() ) { // check to make sure we aren't on the front end
		add_filter('wpseo_pre_analysis_post_content', 'twc_add_custom_to_yoast');

		function nl_add_custom_to_yoast( $content ) {
			global $post;
			$pid = $post->ID;
			
			$custom = get_post_custom($pid);
			unset($custom['_yoast_wpseo_focuskw']); // Don't count the keyword in the Yoast field!

			foreach( $custom as $key => $value ) {
				if( substr( $key, 0, 1 ) != '_' && substr( $value[0], -1) != '}' && !is_array($value[0]) && !empty($value[0])) {
				  $custom_content .= $value[0] . ' ';
				}
			}

			$content = $content . ' ' . $custom_content;
			return $content;

			remove_filter('wpseo_pre_analysis_post_content', 'twc_add_custom_to_yoast'); // don't let WP execute this twice
		}
	}



	// Hide Customizr completely
	// https://wordpress.org/support/topic/feature-request-one-line-of-code-to-disable-all-customizer-stuff
	/***
		* All code below prevents access or hides the WordPress Customizer ( Front-end Editor ) Needed? see,
		* https://codex.wordpress.org/Theme_Customization_API
	***/
	function shmoo_customize() {
		// Disallow acces to an empty editor
		wp_die( sprintf( __( 'No WordPress Theme Customizer support - If needed check your functions.php' ) ) . sprintf( '<br /><a href="javascript:history.go(-1);">Go back</a>' ) );
	}
	add_action( 'load-customize.php', 'shmoo_customize' );

	// Remove 'Customize' from Admin menu
	function remove_submenus() {
		global $submenu;
		// Appearance Menu
		unset($submenu['themes.php'][6]); // Customize
	}
	add_action('admin_menu', 'remove_submenus');

	// Remove 'Customize' from the Toolbar -front-end
	function remove_admin_bar_links() {
	    global $wp_admin_bar;
	    $wp_admin_bar->remove_menu('customize');
	}
	add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

	// Add Custom CSS to Back-end head
	function shmoo_admin_css() {
		echo '<style type="text/css">#customize-current-theme-link { display:none; } </style>';
	}
	add_action('admin_head', 'shmoo_admin_css');


?>