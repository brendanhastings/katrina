<?php 

	// ----
	// Custom Type for Chapters
	// ----

	$labels = array(
		'name'                => _x( 'Chapters', 'Post Type General Name', 'twc' ),
		'singular_name'       => _x( 'Chapter', 'Post Type Singular Name', 'twc' ),
		'menu_name'           => __( 'Chapters', 'twc' ),
		'name_admin_bar'      => __( 'Chapter', 'twc' ),
		'parent_item_colon'   => __( 'Parent Chapter:', 'twc' ),
		'all_items'           => __( 'All Chapters', 'twc' ),
		'add_new_item'        => __( 'Add New Chapter', 'twc' ),
		'add_new'             => __( 'Add New', 'twc' ),
		'new_item'            => __( 'New Chapter', 'twc' ),
		'edit_item'           => __( 'Edit Chapter', 'twc' ),
		'update_item'         => __( 'Update Chapter', 'twc' ),
		'view_item'           => __( 'View Chapter', 'twc' ),
		'search_items'        => __( 'Search Chapter', 'twc' ),
		'not_found'           => __( 'Not found', 'twc' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'twc' ),
	);
	$rewrite = array(
		'slug'                => '',
		'with_front'          => false
	);
	$args = array(
		'label'               => __( 'chapter', 'twc' ),
		'description'         => __( 'The Chapter post type.', 'twc' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'excerpt', 'thumbnail', 'menus'),
		'taxonomies'          => array( 'category', 'post_tag' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-align-left',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,		
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type( 'chapter', $args );
 

 ?>