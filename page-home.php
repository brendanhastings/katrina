<?php

/*
 * Template Name: Home Page
 * Description: A Page Template with particular fields for Home.
 */



$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['first_slide'] = get_field('first_slide', 'options');

Timber::render('pages/page-home.twig', $context);

?>